<?php
/** @noinspection PhpUnnecessaryStaticReferenceInspection */

namespace Drupal\CacheableTypes;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableDependencyTrait;

/**
 * Immutable cacheable string with no surprises.
 *
 * It also makes it hard to forget cacheability.
 */
final class CacheableString implements CacheableDependencyInterface {

  use CacheableDependencyTrait;

  protected string $value;

  private function __construct(string $value, CacheableDependencyInterface $cacheability) {
    $this->value = $value;
    $this->setCacheability($cacheability);
  }

  public static function create(string $value, CacheableDependencyInterface $cacheability): CacheableString {
    return new static($value, $cacheability);
  }

  public function value(): string {
    return $this->value;
  }

}
