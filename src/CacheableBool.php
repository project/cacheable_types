<?php
/** @noinspection PhpUnnecessaryStaticReferenceInspection */

namespace Drupal\CacheableTypes;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableDependencyTrait;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Immutable cacheable boolean with no surprises.
 *
 * It also makes it hard to forget cacheability.
 */
final class CacheableBool implements CacheableBoolInterface {

  use CacheableDependencyTrait;

  protected bool $value;

  private function __construct(bool $value, CacheableDependencyInterface $cacheability) {
    $this->value = $value;
    $this->setCacheability($cacheability);
  }

  public static function create(bool $value, CacheableDependencyInterface $cacheability): CacheableBool {
    return new static($value, $cacheability);
  }

  /**
   * @deprecated in cacheable_types 1.0.4, will be removed before 2.0.0.
   */
  public static function createTrue(): CacheableBool {
    @trigger_error('CacheableBool::createTrue is deprecated in cacheable_types 1.0.4, will be removed before 2.0.0.', E_USER_DEPRECATED);
    return new static(TRUE, new CacheableMetadata());
  }

  /**
   * @deprecated in cacheable_types 1.0.4, will be removed before 2.0.0.
   */
  public static function createFalse(): CacheableBool {
    @trigger_error('CacheableBool::createFalse is deprecated in cacheable_types 1.0.4, will be removed before 2.0.0.', E_USER_DEPRECATED);
    return new static(FALSE, new CacheableMetadata());
  }

  /**
   * @deprecated in cacheable_types 1.0.4, will be removed before 2.0.0.
   *   Use ::ifAccessResultAllowed, ::ifAccessResultNotForbidden
   */
  public static function fromAccessResult(AccessResultInterface $accessResult, bool $useNotForbidden = FALSE): CacheableBool {
    @trigger_error('CacheableBool::fromAccessResult is deprecated in cacheable_types 1.0.4, will be removed before 2.0.0. Use ::ifAccessResultAllowed, ::ifAccessResultNotForbidden', E_USER_DEPRECATED);
    $bool = $useNotForbidden ?
      !$accessResult->isForbidden() :
      $accessResult->isAllowed();

    $cacheability = $accessResult instanceof CacheableDependencyInterface
      ? $accessResult : NULL;
    return new static($bool, $cacheability);
  }

  public static function ifAccessResultAllowed(AccessResultInterface $accessResult): CacheableBool {
    $bool = $accessResult->isAllowed();
    $cacheability = $accessResult instanceof CacheableDependencyInterface
      ? $accessResult : NULL;
    return new static($bool, $cacheability);
  }

  public static function ifAccessResultNotForbidden(AccessResultInterface $accessResult): CacheableBool {
    $bool = !$accessResult->isForbidden();
    $cacheability = $accessResult instanceof CacheableDependencyInterface
      ? $accessResult : NULL;
    return new static($bool, $cacheability);
  }

  /**
   * @deprecated in cacheable_types 1.0.4, will be removed before 2.0.0.
   *   Use ::toAllowedOrNeutral, ::toNeutralOrForbidden, ::toAllowedOrForbidden
   */
  public function toAccessResult(bool $useNotForbidden = FALSE): AccessResult {
    @trigger_error('CacheableBool::toAccessResult is deprecated in cacheable_types 1.0.4, will be removed before 2.0.0. Use ::toAllowedOrNeutral, ::toNeutralOrForbidden, ::toAllowedOrForbidden', E_USER_DEPRECATED);
    $accessResult = $useNotForbidden ?
      AccessResult::forbiddenIf(!$this->value) :
      AccessResult::allowedIf($this->value);
    $accessResult->addCacheableDependency($this);
    return $accessResult;
  }

  public function toAllowedOrNeutral(): AccessResult {
    $accessResult = $this->value ? AccessResult::allowed() : AccessResult::neutral();
    $accessResult->addCacheableDependency($this);
    return $accessResult;
  }

  public function toNeutralOrForbidden(): AccessResult {
    $accessResult = $this->value ? AccessResult::neutral() : AccessResult::forbidden();
    $accessResult->addCacheableDependency($this);
    return $accessResult;
  }

  public function toAllowedOrForbidden(): AccessResult {
    $accessResult = $this->value ? AccessResult::allowed() : AccessResult::forbidden();
    $accessResult->addCacheableDependency($this);
    return $accessResult;
  }

  public static function and(CacheableBool ...$items): CacheableBool {
    foreach ($items as $item) {
      // Any false value makes the result false, and determines its cacheability.
      // I.e. only its changing can make the result change.
      if (!$item->value()) {
        return $item;
      }
    }
    // All true, so any value changing will change the result.
    $cacheability = (new CacheableMetadata());
    foreach ($items as $item) {
      $cacheability->addCacheableDependency($item);
    }
    return static::create(TRUE, $cacheability);
  }

  public static function or(CacheableBool ...$items): CacheableBool {
    foreach ($items as $item) {
      // Any true value makes the result true, and determines its cacheability.
      // I.e. only its changing can make the result change.
      if ($item->value()) {
        return $item;
      }
    }
    // All false, so any value changing will change the result.
    $cacheability = (new CacheableMetadata());
    foreach ($items as $item) {
      $cacheability->addCacheableDependency($item);
    }
    return static::create(FALSE, $cacheability);
  }

  public static function not(CacheableBool $item): CacheableBool {
    return new static(!$item->value(), $item);
  }

  public function value(): bool {
    return $this->value;
  }

}
