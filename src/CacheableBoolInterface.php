<?php

declare(strict_types=1);
namespace Drupal\CacheableTypes;

use Drupal\Core\Cache\CacheableDependencyInterface;

interface CacheableBoolInterface extends CacheableDependencyInterface {

  public function value(): bool;

}
