# TODO

This is TODO and NOT implemented yet.

```php
# Variadic permission convenience
CacheableBool::ifHasPermissions('view any node');
CacheableBool::ifHasPermissions('view any node', 'administer nodes');
```

```php
# Lazy CacheableBool
LazyCacheableBool::create(fn() => $someService->getBool(), $cachability);
LazyCacheableBool::and(...);
```

```php
# Immutable cacheability with type-checked constructor.
Cacheability::create($cacheableDependency)
assert($cacheability instanceof Cacheability)
assert($cacheability instanceof CacheableDependencyInterface)
```

```php
# Immutable collection.
# To enforce integer keys use CacheableList.
# Not iterable to encourage functional approaches.
# Subclass this to typehint items.
# Use psalm generics, T instanceof CacheableValue
CacheableCollection::create($listCacheability, T ...$items)
assert($cacheableCollection instanceof CacheableDependencyInterface)
$cacheableCollection->value(): CacheableCollection
$cacheableCollection->hasItems(): CacheableBool
$cacheableCollection->map(fn(T $item): T, $cacheabilityToAdd): CacheableCollection
# No surprises, re-key must be explicit.
$cacheableCollection->mapAndRekey(fn(T $item, string &$key): T): CacheableCollection
$cacheableCollection->filter(fn(T $item): CacheableBool): CacheableCollection
# Leverage provided list cacheability.
$cacheableCollection->any(fn(T $item): CacheableBool): CacheableBool
$cacheableCollection->every(fn(T $item): CacheableBool): CacheableBool
```

## Maybe someday

```php
# Cacheability builder.
Cacheability::builder($cacheableDependency)
assert($cacheability instanceof CacheabilityBuilder)
# Can not set arbitrary cacheability, only add.
$cacheabilityBuilder->addCacheableDependency($cacheableDependency)
$cacheabilityBuilder->addCacheTags(...)
$cacheabilityBuilder->addCacheContexts(...)
$cacheabilityBuilder->addCacheMaxAge(...)
# Get the value only after freezing.
assert(!$cacheability instanceof CacheableDependencyInterface)
assert($cacheabilityBuilder->toImmutable() instanceof CacheableDependencyInterface)
```

```php
# Immutable cacheable Integer.
# Do we need a builder? Currently, not.
# Do we need ->equals and ->greaterOrEqual and friends?
CacheableInt::create($int, $cacheability)
assert($cacheableInt instanceof CacheableDependencyInterface)
$cacheableInt->value()
```

