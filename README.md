# Drupal.CacheableTypes

Immutable classes and builders that make doing it wrong harder.

## CacheableBool

```php
# Never forget cacheability.
CacheableBool::create($bool, $cacheability)
$cacheableBool->value()
assert($cacheableBool instanceof CacheableDependencyInterface)
CacheableBool::ifAccessResultAllowed($accessResult)
CacheableBool::ifAccessResultNotForbidden($accessResult)
$cacheableBool->toAllowedOrNeutral()
$cacheableBool->toNeutralOrForbidden()
$cacheableBool->toAllowedOrForbidden()
CacheableBool::not()
CacheableBool::and(...$cacheableBoolItems)
CacheableBool::or(...$cacheableBoolItems)
```
